import os

DEBUG = False
ALLOWED_HOSTS = ["claudio-santos.com", "simplologia.com.br", "localhost", ]
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECRET_KEY = 'production'

ROOT_URLCONF = "website.urls"

INSTALLED_APPS = (
    'django.contrib.staticfiles',
)

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, "templates"),
        ]
    },
]

WSGI_APPLICATION = 'website.wsgi.application'

STATIC_URL = '/static/'
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
]
