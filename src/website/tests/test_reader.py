from django.test import SimpleTestCase
from ..reader import FolderExplorer


class TestReader(SimpleTestCase):

    def test_must_read_thoughts(self):
        xavier = FolderExplorer(path='tests/data/thoughts')
        thoughts = xavier.read()
        self.assertEqual(len(thoughts), 2)

    def test_must_read_summary_thoughts(self):
        xavier = FolderExplorer(path='tests/data/thoughts')
        thoughts = xavier.read()
        title = ''
        summary = ''
        for thought in thoughts:
            if thought.name == 'my-first-post':
                title = thought.title
                summary = thought.summary
        self.assertEqual(title, 'My First Post')
        self.assertEqual(summary, 'My First Summary')        