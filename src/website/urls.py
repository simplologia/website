from django.conf.urls import url
from .views import Thoughts, Thought, Portifolios, Portifolio, AboutMe


urlpatterns = [
    url(r'^about-me/$', AboutMe.as_view()),
    url(r'^thoughts/$', Thoughts.as_view()),
    url(r'^thought/(?P<name>[\w-]+)/*$', Thought.as_view()),
    url(r'^portifolios/$', Portifolios.as_view()),
    url(r'^portifolio/(?P<name>[\w-]+)/*$', Portifolio.as_view()),
    url(r'', Thoughts.as_view()),
]
