import datetime
import os
import re
from os import scandir
import markdown
from django.conf import settings


class FileReader():
    
    def __init__(self, file_path, name):
        self.name = name
        self.path = file_path
        self.title = ''
        self.summary = ''
        self.default_pattern = r"^[title|summary]+:\s*([\w\s\-\(\)\[\]/'/?/!/./,/*/#]+)$"

    def read_file_property(self):
        with open(self.path, 'r') as file:
            title_line = file.readline().replace('\n', '').strip()
            self.title = re.findall(self.default_pattern, title_line).pop()

            summary_line = file.readline().replace('\n', '').strip()
            self.summary = re.findall(self.default_pattern, summary_line).pop()
            print(summary_line)
            print("leu tudo")


class FolderExplorer():
    def __init__(self, path):
        self.source_path = os.path.join(settings.BASE_DIR, path)
        self.name_pattern = r'^([\w-]+).md$'

    def read(self):
        works = scandir(self.source_path)
        thoughts = []
        for file in works:
            if not file.is_file():
                continue
            try:
                print(file.name)
                name = re.findall(self.name_pattern, file.name)
                print(name)
                thoughts.append(Post(
                    name.pop().strip(),
                    file.path,
                    datetime.datetime.fromtimestamp(file.stat().st_mtime)))
            except Exception as err:
                print(err)

        return thoughts

    def read_file(self, name):
        file_name = name + '.md'
        with open(self.source_path + '/' + file_name, mode="r", encoding="utf-8") as file:
            lines = file.readlines()
            return markdown.markdown(''.join(lines[2:]))


class Post(FileReader):

    def __init__(self, name, file_path, last_update):
        super().__init__(file_path, name)
        self.last_update = last_update
        super().read_file_property()


