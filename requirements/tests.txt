-r base.txt
coverage
mock>=1.0.1
flake8>=2.1.0
django-test-without-migrations
tblib
django-fake-model